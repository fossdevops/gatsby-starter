ARG IMAGE_VERSION=${PHP_IMAGE_VERSION:-lts-alpine}
ARG REGISTRY_URI=${REGISTRY_URI:-}
ARG BRANCH_NAME=${BRANCH_NAME:-latest}
FROM node:${IMAGE_VERSION} as node_base

USER 0
ARG USERNAME=${USERNAME:-node}
ARG GROUPNAME=${USERNAME:-${USERNAME}}
ARG CREATE_GROUP=${CREATE_GROUP:-false}
ARG CREATE_USER=${CREATE_USER:-false}
ARG UID=${UID:-1000}
ARG GID=${GID:-1000}
# Need a place to write npm settings junk
ARG USER_HOME_DIR=${USER_HOME_DIR:-/home/${USERNAME}}

RUN cat /etc/passwd

# create a user for the web app
RUN if [ 'true' = "${CREATE_GROUP}" ]; then \
      addgroup -g "${GID}" -S "${GROUPNAME}"; \
    fi && \
    if [ 'true' = "${CREATE_USER}" ]; then \
      adduser -h "${USER_HOME_DIR}" -g "${USERNAME}" -s /sbin/nologin \
              -u "${UID}" -G "${GROUPNAME}" -S -D \
              -H -k /dev/null "${USERNAME}"; \
    fi

ARG SITE_ROOT=${SITE_ROOT:-/var/www-data}

# Create the site root and give ownership to the site user
RUN mkdir -p "${USER_HOME_DIR}" && \
    chown -R "${UID}:${GID}" "${USER_HOME_DIR}" && \
    mkdir -p "${SITE_ROOT}" && \
    chown -R "${UID}:${GID}" "${SITE_ROOT}"

# Switch to the site root directory
WORKDIR ${SITE_ROOT}

# set C.UTF-8 locale as default.
ENV LANG=C.UTF-8

ARG ALPINE_GLIBC_BASE_URL=${ALPINE_GLIBC_BASE_URL:-https://github.com/sgerrand/alpine-pkg-glibc/releases/download}
ARG ALPINE_GLIBC_PACKAGE_VERSION=${ALPINE_GLIBC_PACKAGE_VERSION:-2.30-r0}
ARG ALPINE_GLIBC_BASE_PACKAGE_FILENAME=${ALPINE_GLIBC_BASE_PACKAGE_FILENAME:-glibc-$ALPINE_GLIBC_PACKAGE_VERSION.apk}
ARG ALPINE_GLIBC_BIN_PACKAGE_FILENAME=${ALPINE_GLIBC_BIN_PACKAGE_FILENAME:-glibc-bin-$ALPINE_GLIBC_PACKAGE_VERSION.apk}
ARG ALPINE_GLIBC_I18N_PACKAGE_FILENAME=${ALPINE_GLIBC_I18N_PACKAGE_FILENAME:-glibc-i18n-$ALPINE_GLIBC_PACKAGE_VERSION.apk}

# Here we install GNU libc (aka glibc)

RUN apk add --no-cache --virtual=.build-dependencies wget ca-certificates && \
    echo \
        "-----BEGIN PUBLIC KEY-----\
        MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApZ2u1KJKUu/fW4A25y9m\
        y70AGEa/J3Wi5ibNVGNn1gT1r0VfgeWd0pUybS4UmcHdiNzxJPgoWQhV2SSW1JYu\
        tOqKZF5QSN6X937PTUpNBjUvLtTQ1ve1fp39uf/lEXPpFpOPL88LKnDBgbh7wkCp\
        m2KzLVGChf83MS0ShL6G9EQIAUxLm99VpgRjwqTQ/KfzGtpke1wqws4au0Ab4qPY\
        KXvMLSPLUp7cfulWvhmZSegr5AdhNw5KNizPqCJT8ZrGvgHypXyiFvvAH5YRtSsc\
        Zvo9GI2e2MaZyo9/lvb+LbLEJZKEQckqRj4P26gmASrZEPStwc+yqy1ShHLA0j6m\
        1QIDAQAB\
        -----END PUBLIC KEY-----" | sed 's/   */\n/g' > "/etc/apk/keys/sgerrand.rsa.pub" && \
    wget \
        "${ALPINE_GLIBC_BASE_URL}/${ALPINE_GLIBC_PACKAGE_VERSION}/${ALPINE_GLIBC_BASE_PACKAGE_FILENAME}" \
        "${ALPINE_GLIBC_BASE_URL}/${ALPINE_GLIBC_PACKAGE_VERSION}/${ALPINE_GLIBC_BIN_PACKAGE_FILENAME}" \
        "${ALPINE_GLIBC_BASE_URL}/${ALPINE_GLIBC_PACKAGE_VERSION}/${ALPINE_GLIBC_I18N_PACKAGE_FILENAME}" && \
    apk add --no-cache \
        "${ALPINE_GLIBC_BASE_PACKAGE_FILENAME}" \
        "${ALPINE_GLIBC_BIN_PACKAGE_FILENAME}" \
        "${ALPINE_GLIBC_I18N_PACKAGE_FILENAME}" && \
    \
    rm "/etc/apk/keys/sgerrand.rsa.pub" && \
    /usr/glibc-compat/bin/localedef --force --inputfile POSIX --charmap UTF-8 "${LANG}" || true && \
    echo "export LANG=${LANG}" > /etc/profile.d/locale.sh && \
    \
    apk del glibc-i18n && \
    \
    rm "/root/.wget-hsts" && \
    apk del .build-dependencies && \
    rm \
        "${ALPINE_GLIBC_BASE_PACKAGE_FILENAME}" \
        "${ALPINE_GLIBC_BIN_PACKAGE_FILENAME}" \
        "${ALPINE_GLIBC_I18N_PACKAGE_FILENAME}"

# Add the ability for user to change file ownership with sudo
#RUN apk add sudo && \
#    mkdir /var/www-data && chown "${USERNAME}:${USERNAME}" /var/www-data && \
#    echo -ne "\napache ALL= NOPASSWD: /bin/chown\n" >> /etc/sudoers

# Add prereqs for ms vscode-server, hooks/husky, and cwebp
RUN apk add musl libgcc libstdc++ openssh bash git mesa-gl
SHELL ["/bin/bash", "-lc"]

# RUN yarn global add --no-lockfile eslint

USER ${USERNAME}
RUN echo -ne '#!/bin/bash\nexport PATH=${HOME}/code/node_modules/.bin:${PATH}\n' > "${USER_HOME_DIR}/.bash_profile"